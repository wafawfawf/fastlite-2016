package com.darryring.fast.activitys;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.darryring.fast.R;
import com.darryring.fast.theme.FastTheme;
import com.darryring.fast.util.DrawableUtil;
import com.darryring.fast.util.InputUtil;
import com.darryring.libcore.Fast;
import com.darryring.libcore.base.BaseAppCompatActivity;
import com.darryring.libmodel.entity.theme.ThemeEntity;

import net.steamcrafted.loadtoast.LoadToast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends BaseAppCompatActivity implements OnClickListener {

    public static String SAVE_LOGIN_STATE = "com.darryring.fast.activitys.LoginActivity.KEY_LOGIN";

    // UI references.
    @BindView(R.id.email)
     AutoCompleteTextView mEmailView;
    @BindView(R.id.password)
     EditText mPasswordView;

    @BindView(R.id.login_progress)
     View mProgressView;
    @BindView(R.id.login_form)
     View mLoginFormView;
    //
    @BindView(R.id.tv_your_photo)
    TextView tvYouPhoto;
    //
    @BindView(R.id.tv_account_weixin)
    TextView tvAccountWeiXin;
    @BindView(R.id.tv_account_weibo)
    TextView tvAccountWeiBo;
    @BindView(R.id.tv_account_qq)
    TextView tvAccountQQ;

    @BindView(R.id.email_sign_in_button)
    Button mEmailSignInButton;
    int themeColor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FastTheme.fastTheme.setupTheme(this, ThemeEntity.TYPE_THEME);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        //-------------------------
        final Drawable _youPhotoDrawable = tvYouPhoto.getBackground();

        //
        final Drawable originalDrawable = mEmailSignInButton.getBackground();

        TypedArray ta = getTheme().obtainStyledAttributes(R.styleable.FastColorTheme);
        int _pressedColor = ta.getColor(R.styleable.FastColorTheme_colorFastPressed,0);
        int _nomailColor = ta.getColor(R.styleable.FastColorTheme_colorFastNomal,0);
        themeColor = _nomailColor;
        //
        ColorStateList colorStateList = DrawableUtil.createButtonColorStateList(_nomailColor,_pressedColor);

        final Drawable wrappedDrawable = DrawableUtil.tintDrawable(originalDrawable,colorStateList);
        mEmailSignInButton.setBackgroundDrawable(wrappedDrawable);
        //
        mEmailSignInButton.setOnClickListener(this);
        //
        final Drawable  _tintYouPhtoDrawable = DrawableUtil.tintDrawable(_youPhotoDrawable,colorStateList);
        tvYouPhoto.setBackgroundDrawable(_tintYouPhtoDrawable);
        //
        final Drawable  _tintWeiXinDrawable = DrawableUtil.tintDrawable(tvAccountWeiXin.getBackground(),colorStateList);
        final Drawable  _tintWeiBoDrawable = DrawableUtil.tintDrawable(tvAccountWeiBo.getBackground(),colorStateList);
        final Drawable  _tintQQDrawable = DrawableUtil.tintDrawable(tvAccountQQ.getBackground(),colorStateList);
        tvAccountWeiXin.setBackgroundDrawable(_tintWeiXinDrawable);
        tvAccountWeiBo.setBackgroundDrawable(_tintWeiBoDrawable);
        tvAccountQQ.setBackgroundDrawable(_tintQQDrawable);

        //
        ta.recycle();
    }


    @OnClick(R.id.input_click_layout) void onClickhideInput() {
        InputUtil.hideInput(LoginActivity.this);
    }

    @Override
    public void onClick(final View view) {
        if(view.getId()==R.id.email_sign_in_button){
            final LoadToast lt = new LoadToast(this).setProgressColor(themeColor).setText("login....").setTranslationY(100).show();
            lt.show();
            view.postDelayed(new Runnable() {
                @Override
                public void run() {
                    lt.success();
                     view.postDelayed(new Runnable() {
                         @Override
                         public void run() {
                             loginOk();
                         }
                     },1000);
                }
            },2000);

//            String _user = mEmailView.getText().toString();
//            String _pw = mPasswordView.getText().toString();
//            if(!StringUtil.isNull(_user)&&!StringUtil.isNull(_pw)){
//                showProgress(true);
//                if(LibChat.chatClient!=null){
//                    LibChat.chatClient.loginAsyn(_user, StringUtil.Md5(_pw), new OkCall() {
//                        @Override
//                        public void ok() {
//                            loginOk();
//                        }
//                        @Override
//                        public void error(int what, String error, Object obj) {
//                            loginError();
//                        }
//                    });
//                }
//
//            }

        }
    }

    private void loginOk() {
        if(!isFinishing()){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Fast.kvCache.saveToFileCache(SAVE_LOGIN_STATE,"true");
                    Intent mIm = new Intent(LoginActivity.this,HomeActivity.class);
                    startActivity(mIm);
                    finish();
                }
            });
        }
    }

    private void loginError() {
        if(!isFinishing()){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                }
            });
        }
    }
}

