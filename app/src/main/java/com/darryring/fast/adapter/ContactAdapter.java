package com.darryring.fast.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.darryring.fast.R;
import com.darryring.libmodel.entity.FContact;

import java.util.List;

/**
 * Created by hljdrl on 16/4/11.
 */
public class ContactAdapter extends ArrayAdapter<FContact> {

    LayoutInflater inflater;
    public ContactAdapter(Context context,List<FContact> objects) {
        super(context, 0, objects);
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View _view, ViewGroup parent) {
        ViewHolder _viewHolder = null;
        if(_view==null){
            _view = inflater.inflate(R.layout.item_contact,null);
            _viewHolder = new ViewHolder();
            _viewHolder.imageview = (ImageView) _view.findViewById(R.id.image_avator);
            _viewHolder.name = (TextView) _view.findViewById(R.id.tv_ct_name);
            _viewHolder.sign = (TextView) _view.findViewById(R.id.tv_ct_sign);
            _view.setTag(_viewHolder);
        }else{
            _viewHolder = (ViewHolder) _view.getTag();
        }
        FContact _ct = getItem(position);
        _viewHolder.name.setText(_ct.getChatId());
        return _view;
    }
    private static class ViewHolder{
        public ImageView imageview;
        public TextView  name;
        public TextView  sign;
    }
}
