package com.darryring.libcore.http;

import java.io.File;
import java.io.IOException;

/**
 * Created by hljdrl on 16/3/2.
 */
public interface Http {

    /**
     * @param url
     * @param json
     * @return
     * @throws IOException
     */
    String post(String url, String json) throws IOException;

    /**
     * @param url
     * @param json
     * @return
     * @throws IOException
     */
    String postJSON(String url,String json)throws IOException;

    /**
     * @param url
     * @param json
     * @return
     * @throws IOException
     */
    byte[] postJSON2(String url,String json)throws IOException;

    /**
     * @param url
     * @param file
     * @return
     * @throws Exception
     */
    String postFile(String url,File file) throws Exception;

    /**
     * @param url
     * @param string
     * @return
     * @throws Exception
     */
    String postString(String url,String string)throws Exception;

    /**
     * @param url
     * @return
     * @throws IOException
     */
    String get(String url)throws IOException;
}
