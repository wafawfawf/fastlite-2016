package com.darryring.libcore.android.file;

import android.os.Environment;

import com.darryring.libcore.file.Files;
import com.darryring.libcore.util.StringUtil;

import java.io.File;

/**
 * Created by hljdrl on 16/4/28.
 */
public class AndroidFiles implements Files {
    String TAG = "AndroidFiles";

    public AndroidFiles(){
        cameraFile();
    }

    @Override
    public File cameraFile() {
        File mFm = null;
        StringBuffer _buf = new StringBuffer();
        if (Environment.getExternalStorageDirectory() != null) {
            _buf.append(Environment.getExternalStorageDirectory());
            _buf.append("/DCIM/Camera/");
            fileMake(_buf.toString());
            mFm = new File(_buf.toString());
        }
        return mFm;
    }
    void fileMake(String file) {
        if (StringUtil.isNotNull(file)) {
            File _mFm = new File(file);
            if (!_mFm.exists()) {
                boolean _var = _mFm.mkdirs();
            }
        }
    }
}
