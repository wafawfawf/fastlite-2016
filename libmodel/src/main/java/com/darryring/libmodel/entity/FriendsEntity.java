package com.darryring.libmodel.entity;

import java.util.List;

/**
 * Created by hljdrl on 16/5/27.
 */
public class FriendsEntity {

    private String icon;
    private String name;
    private String body;
    private String time;
    private List<String> images;

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }
}
