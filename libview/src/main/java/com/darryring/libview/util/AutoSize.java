//package com.darryring.libview.util;
//
//import android.content.Context;
//import android.content.res.Resources;
//import android.util.DisplayMetrics;
//import android.view.Display;
//import android.view.WindowManager;
//
//import com.darryring.libcore.FastLite;
//
//
///**
// * Created by hljdrl on 16/3/23.
// */
//public class AutoSize {
//
//    private static final  String TAG="AutoSize";
//    private static int mScreenWidth;
//    private static int mScreenHeight;
//    //1920x1080
//    private static int mSourceWidth=1080;
//    private static int mSourceHeight=1920;
//    public static void init(Context context){
//        WindowManager _wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
//        Display _display = _wm.getDefaultDisplay();
//        mScreenWidth = _display.getWidth();
//        mScreenHeight = _display.getHeight();
//        FastLite.logger.i(TAG,"DisplayWidth:"+mScreenWidth);
//        FastLite.logger.i(TAG,"DisplayHeight:"+mScreenHeight);
//    }
//    public final static int getScreenWidth(){
//        return mScreenWidth;
//    }
//    public final static int getScreenHeight(){
//        return mScreenHeight;
//    }
//
//    public static TakeSize autoSize(int sourceWidth, int sourceHeight){
//        TakeSize _size = new TakeSize();
//        _size.sourceWidth = sourceWidth;
//        _size.sourceHeight = sourceHeight;
//        _size.takeWidth = takeValue(sourceWidth,mScreenWidth,mSourceWidth);
//        _size.takeHeight = takeValue(sourceHeight,mScreenHeight,mSourceHeight);
//        return _size;
//    }
//    private static int takeValue(int sourceValue,int screenDisplay,int sourceDisplay){
//        float _take;
//        float _value = sourceValue;
//        float _sd = screenDisplay;
//        float _se = sourceDisplay;
//        float _sace =(_se/_sd);
//        _take = (_sace*_value);
//        return (int)_take;
//    }
//
//    public static TakeSize autoSize(int sourceWidth){
//        return autoSize(sourceWidth,sourceWidth);
//    }
//}
